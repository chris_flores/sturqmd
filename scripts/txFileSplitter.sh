#!/bin/bash

inputFile=$1
nEventsPerFile=$2
outDir=$3

nFile=0
eventCounter=0
newFileName=$outDir/$(basename $inputFile .tx)

#If no output directory was supplied then assume the same directory
#as the input file
if [ -z $outDir ]
then
    newFileName=$(dirname $inputFile)/$(basename $inputFile .tx)
fi

while read line
do

    if [ $(echo "$line" | grep -c --no-filename "EVENT") -eq 1  ]
    then 
	eventCounter=$((eventCounter + 1))
    fi
    
    echo "$line" >> "$newFileName"_$nFile".tx"
	
    

    if [ $eventCounter -eq $nEventsPerFile ]
    then
	eventCounter=0
	nFile=$((nFile + 1))
	echo "Finished File $nFile"
    fi

done < $inputFile

