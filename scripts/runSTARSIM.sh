#!/bin/bash

#Run STARSIM
#This must be executed on RCF or somewhere that has the necessary STAR databases and libraries

#Variables from the User
outDir=$1
logDir=$2
schedDir=$3
energyName=$4
inputFileList=$5
geometry=$6

#Check to make sure there are the right number of arguments
if [ "$#" -ne 6 ]; then
    echo "ERROR: Incorrect number of arguments! This script needs six: outputDir logDir schedDir energyName inputFileList geometry"
    exit 1
fi

#Check to Make sure the output Directory exists
if [ ! -d $outDir ]; then
    echo "ERROR: Output Directory $outDir does not exist!"
    exit 1
fi

#Check to make sure the energy name is not empty
if [ -z "$energyName" ]; then
    echo "ERROR: Energy name is empty!"
    exit 1
fi

#Check to make sure that the inputFileList exists 
if [ ! -e $inputFileList ]; then
    echo "ERROR: Input file list $inputFileList does not exist!"
    exit 1
fi

#Check that the log directory exists, if not make it
if [ ! -d $logDir ]; then
    echo "Creating the log directory $logDir"
    mkdir $logDir
fi

#Check that the scheduler directory exists, if not make it
if [ ! -d $schedDir ]; then
    echo "Creating the scheduler directory $schedDir"
    mkdir $schedDir
fi

#The STARSIM kumac to be used from this repo
kumac=$PWD/../STARSIM/StarSim.kumac

#Check to make sure the Kumac file exists
if [ ! -e $kumac ]; then
    echo "ERROR: The STARSIM kumac file $kumac does not exist"
    exit 1
fi

#Finally run STARSIM
star-submit-template -template ../STARSIM/runSTARSIM.xml -entities logDir=$logDir,energyName=$energyName,listOfInputFiles=$inputFileList,outDir=$outDir,schedDir=$schedDir,geometry=$geometry,kumac=$kumac
