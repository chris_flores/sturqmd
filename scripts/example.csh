#!/bin/csh
# -------------------------------------- 
# Script generated  at Sat Jan 07 23:02:11 PST 2017 by the STAR scheduler and submitted with
# cd /global/homes/c/cflores/sturqmd/scripts; qsub -t 1-1:1 array_0_0_B237C46214EA40A4DD0ECB4E2EA192CD.csh
# --------------------------------------

#$ -l h_vmem=2G,projectlio=1

if ( ! $?SGE_TASK_ID ) then
   echo 'Error: No $SGE_TASK_ID defined'
   exit
else if ($SGE_TASK_ID == 1) then
    /bin/touch /project/projectdirs/star/pwg/starlfs/cflores/SpectraBackground/ColliderCenter/AuAu14//logs/BFC_AuAu14_0.out
    /bin/touch /project/projectdirs/star/pwg/starlfs/cflores/SpectraBackground/ColliderCenter/AuAu14//logs/BFC_AuAu14_0.err
    (( /global/homes/c/cflores/sturqmd/scripts/../../sched/schedB237C46214EA40A4DD0ECB4E2EA192CD_0.csh ) >> /project/projectdirs/star/pwg/starlfs/cflores/SpectraBackground/ColliderCenter/AuAu14//logs/BFC_AuAu14_0.out ) >>& /project/projectdirs/star/pwg/starlfs/cflores/SpectraBackground/ColliderCenter/AuAu14//logs/BFC_AuAu14_0.err
endif
