#!/bin/bash

dir=$1                     #Directory with root files to process
outStarsimDir=$2           #Directory where the .tx files should be stored
vertexHistoFile=$3         #path to and name of root file with vertex histos
vertexHistoBaseName=$4     #base name of the histos (histos need to be named something like: basename_zVertex)
maxEventsPerOutputFile=$5  #if there are more than this many events in the urqmd file they will be split 
                           #across multiple starsim files
nDesiredEventsPerJob=$6    #the number of desired events you would like from each job (e.g. because not all
                           #events in the root file will be valid events for the tx file)

#Check for the right number of arguments
if [ "$#" -ne 6 ]; then
    echo "ERROR: Incorrect number of arguments...there should be six!"
    exit 1
fi

#Check that the input Directory exists
if [ ! -d $dir ];then
    echo "ERROR: Input file directory $dir does not exist!"
    exit 1
fi

#Check that the output starsim directory exists
if [ ! -d $outStarsimDir ]; then
    echo "ERROR: The output STARSIM directory $outStarsimDir does not exist"
    exit 1
fi

#Check that the Vertex Histo File exists
if [ ! -e $vertexHistoFile ]; then
    echo "WARNING: The vertex histogram file $vertexHistoFile does not exist!"
    echo "         Event vertices will all be defined as (0,0,0)"
fi

fileArray=( $dir/*.root )
processIDs=()

maxJobs=6
currentJobs=0
completedJobs=0

while [ $completedJobs -lt ${#fileArray[@]} ]
do

    if [[ $currentJobs -lt $maxJobs ]]
    then
	root -l -q -b ../macros/RunMakeStarsimTxFiles.C\(\"${fileArray[$completedJobs]}\",\"$outStarsimDir\",\"$vertexHistoFile\",\"$vertexHistoBaseName\",$maxEventsPerOutputFile,$nDesiredEventsPerJob\) &

	processIDs+=($!)
	echo "Submitting Job $completedJobs"
	completedJobs=$(($completedJobs + 1))
    
    else
	sleep 15
	
    fi
        
    currentJobs=`ps --no-headers -o pid --ppid=$$ | wc -w`
    
done

#Make sure all the jobs have completed
wait ${processIDs[@]}


