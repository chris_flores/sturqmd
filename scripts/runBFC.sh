#!/bin/bash

#Run The STAR Reconstruction Chain, BFC
#This must be executed on RCF or somewhere that has the necessary STAR databases and libraries

#Variables from the User
outDir=$1
logDir=$2
schedDir=$3
energyName=$4
inputFileList=$5
config=$6
first=$7
last=$8

#Check to make sure there are the right number of arguments
if [ "$#" -ne 8 ]; then
    echo "ERROR: Incorrect number of arguments! This script needs eight: outputDir logDir schedDir energyName inputFileList configuration first last"
    exit 1
fi

#Check to make sure the config variable is set
if [ -z $config ]; then
    echo "ERROR: The configuration variable is not set!"
    exit 1
fi

#Get the Star Library version and Production Chain Options from the Config file
source ../BFC/bfc.config $config

if [ "$?" != 0 ]; then
    echo "ERROR: BFC Configuration was not successfull!"
    exit 1
fi

if [ -z "$starVersion" ]; then
    echo "ERROR: STAR Version not properly set!"
    exit 1
fi

if [ -z "$chainOptions" ]; then
    echo "ERROR: Chain Options not properly set!"
    exit 1
fi

#Check to Make sure the output Directory exists
if [ ! -d $outDir ]; then
    echo "ERROR: Output Directory $outDir does not exist!"
    exit 1
fi

#Check to make sure the energy name is not empty
if [ -z "$energyName" ]; then
    echo "ERROR: Energy name is empty!"
    exit 1
fi

#Check to make sure that the inputFileList exists 
if [ ! -e $inputFileList ]; then
    echo "ERROR: Input file list $inputFileList does not exist!"
    exit 1
fi

#Check that the log directory exists, if not make it
if [ ! -d $logDir ]; then
    echo "Creating the log directory $logDir"
    mkdir $logDir
fi

#Check that the scheduler directory exists, if not make it
if [ ! -d $schedDir ]; then
    echo "Creating the scheduler directory $schedDir"
    mkdir $schedDir
fi

echo $starVersion
echo $chainOptions

#Finally run the BFC
star-submit-template -template ../BFC/runBFC.xml -entities logDir=$logDir,energyName=$energyName,listOfInputFiles=$inputFileList,outDir=$outDir,schedDir=$schedDir,starVersion=$starVersion,chainOptions="$chainOptions",first=$first,last=$last
