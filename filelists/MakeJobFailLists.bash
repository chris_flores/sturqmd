#!/bin/bash

for file in *TX*; do 
    
    fileName=$(basename $file .list | cut -d"_" -f1-2)
    
    txDir=$(dirname $(head -n 1 $file))

    dirVer=$(echo $txDir | cut -d"_" -f2 | cut -f1 | tr -d '\n')
    if ! [[ $dirVer =~ ^[0-9]+$ ]]; then
	dirVer=""
    else
	dirVer=_$dirVer
    fi

    starsimDir=$(echo $txDir | rev | cut -d"/" -f2-)
    starsimDir=$(echo $starsimDir | rev)
    bfcDir=$starsimDir/BFC$dirVer
    starsimDir=$starsimDir/STARSIM$dirVer

    bfcFinishedName=$fileName\_BFC_Finished$dirVer.list
    bfcFailFileName=$fileName\_BFC_Fails$dirVer.list
    starsimFinishedName=$fileName\_STARSIM_Finished$dirVer.list
    starsimFailFileName=$fileName\_STARSIM_Fails$dirVer.list
   
    touch $bfcFailFileName
    touch $starsimFailFileName

    ls -1 $starsimDir/*.fzd > $starsimFinishedName 
    ls -1 $bfcDir/*minimc.root > $bfcFinishedName
    
    #Check if the total number of files in the bfc directory
    #is the same as the number of files in the tx directory.
    #If so then they all completed and there is no need to continue.
    if [ $(cat $bfcFinishedName | wc -l) -eq $(ls -1 $txDir/*.tx | wc -l) ]; then
	echo "All Files in $file completed!"
	rm $starsimFinishedName
	rm $bfcFinishedName
	rm $bfcFailFileName
	rm $starsimFailFileName
	continue
    fi
    
    while read line; do
	name=$(basename $line .tx)
	number=$(grep -c $name $bfcFinishedName)
	if [ $number -eq 0 ]; then
	    echo $starsimDir/$name.fzd >> $bfcFailFileName
	    txnumber=$(grep -c $name $starsimFinishedName)
	    if [ $txnumber -eq 0 ]; then
		echo $txDir/$name.tx >> $starsimFailFileName
	    fi
	fi
    done < $file

    nFails=$(cat $bfcFailFileName | wc -l)
    if [ $nFails -eq 0 ]; then
	rm $bfcFinishedName
	rm $bfcFailFileName
	rm $starsimFailFileName
	rm $starsimFinishedName
    elif [ $(cat $starsimFailFileName | wc -l) -eq 0 ]; then
	rm $starsimFailFileName
	rm $starsimFinishedName
    fi
    
done
